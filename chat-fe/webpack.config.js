const path = require('path');

module.exports = {
	mode: 'development',
	entry: './src/index.js',
	output: {
		path: path.resolve(__dirname, 'build')
	},
	module: {
		rules: [{
			test:/\.js$/,
			exclude: /node_modules/,
			loader: 'babel-loader'
		}]
	},
	devtool: 'eval-source-map',
	devServer: {
		contentBase: path.join(__dirname, 'build'),
		compress: true,
		port: 3330
	}
};