const MIME_JSON = 'application/json';

const REST_REQUEST_TEMPLATE = (method) => ({
	headers: {
		'Accept': MIME_JSON,
		'Content-Type': MIME_JSON
	},
	method
});

export const fetchBuilder = (method) => (url) => {
	let requestOptions = REST_REQUEST_TEMPLATE(method),
		convertToJson = false;
	return {
		withBody(data) {
			requestOptions = {...requestOptions, ...{body: JSON.stringify(data)}};
			return this;
		},
		jsonResponse() {
			convertToJson = true;
			return this;
		},
		fetch() {
			return fetch(url, requestOptions)
				.then(convertToJson ? (data) => data.json() : (param) => param);
		}
	};
};

export const get = fetchBuilder('GET');
export const post = fetchBuilder('POST');