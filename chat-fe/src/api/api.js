import { MESSAGE, MESSAGES, USERS } from './endpoints';
import { get, post } from './fetchBuilder';

export const getUsers = () => get(USERS).jsonResponse().fetch();
export const registerUser = (nickname) => post(USERS).withBody({nickname}).jsonResponse().fetch();
export const getMessages = (from, to) => get(MESSAGE(from, to)).jsonResponse().fetch();
export const sendMessage = (from, to, text) => post(MESSAGES).withBody({from, to, text}).fetch();