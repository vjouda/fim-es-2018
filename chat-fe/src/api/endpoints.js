const API_PATH = 'https://chat-be.herokuapp.com';

export const USERS = `${API_PATH}/users`;
export const MESSAGES = `${API_PATH}/messages`;
export const MESSAGE = (from, to) => `${MESSAGES}/${from}/${to}`;