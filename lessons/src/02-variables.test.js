describe('Variables', () => {
	test("variable 'school' should be declared", () => {

		expect(school).toBeUndefined();
	});
	test("variable 'color' should be 'blue'", () => {
		var color;

		expect(color).toBe('blue');
	});
	test("variable 'age' should not be modifiable", () => {
		var age = 20;

		expect(() => {
			age = 30;
		}).toThrow();
	});
	test("variable 'block' should be block-scoped", () => {
		{
			var block = 'I should really be block-scoped';
		}

		expect(() => block).toThrow();
	});
});