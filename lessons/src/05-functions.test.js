describe('Functions', () => {
    test("function 'add' should add 2 numbers", () => {

    	expect(add(1,1)).toBe(2);
    });
	test("function 'sub' should contain property 'type'", () => {
		let sub;

		expect('type' in sub).toBeTruthy();
	});
	test("function 'curry' should accept one param and return another function which returns that param when invoked", () => {

		expect(curry('param')()).toBe('param');
	});
	test("function 'callback' should accept another function, call it when invoked and return its return value", () => {

		expect(callback(() => 'returnVal')).toBe('returnVal');
	});
});