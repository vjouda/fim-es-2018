describe('Objects', () => {
    test("variable 'car' should be an object ", () => {
        var car;

        expect(typeof car).toBe('object');
    });
	test("variable 'car' should be an object with property 'speed'", () => {
		var car;

		expect('speed' in car).toBeTruthy();
	});
});