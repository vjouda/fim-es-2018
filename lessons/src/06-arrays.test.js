function car(brand, speed, color){
	return {brand, speed, color};
}

let cars = [
	car('Tesla', 200, 'black'),
	car('Skoda', 160, 'red'),
	car('Audi', 220, 'black'),
	car('Seat', 180, 'yellow'),
];

describe('Arrays', () => {
	test("variable 'numbers' should contain array of numbers from 0 to 2 in order", () => {
		// you cannot edit next line
		let numbers = [1, 2];

		expect(numbers).toEqual([0, 1, 2]);

	});
	test("variable 'blackCars' should contain only black cars", () => {
		let blackCars = cars;

		expect(blackCars).toEqual([cars[0], cars[2]]);
	});
	test("variable 'brands' should contain array of all car brands as string", () => {
		let brands = cars;

		expect(brands).toEqual(['Tesla', 'Skoda', 'Audi', 'Seat']);
	});
	test("variable 'totalSpeed' should be equal to sum of all cars speed", () => {
		let totalSpeed = cars;

		expect(totalSpeed).toEqual(760);
	});
	test("variable 'bySpeed' should contain array of cars sorted by top speed", () => {
		let bySpeed = cars;

		expect(cars).toEqual([cars[2], cars[0], cars[3], cars[1]]);
	});
});