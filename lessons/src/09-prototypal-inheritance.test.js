var phone = {
	displayType: 'AMOLED',
	cpu: {
		model: 'Snapdragon 821',
		cores: 8
	}
};

function SamsungPhone(){
	this.systemVersion = '3.0';
}

SamsungPhone.prototype = phone;

var samsungPhone = new SamsungPhone();

describe('Given example above', function(){
	it('samsungPhone should have systemVersion property ', function(){
		expect(samsungPhone.systemVersion).toBeDefined();
	});
	it('samsungPhone should have cpu property inherited from its prototype ', function(){
		expect(samsungPhone.cpu).toBeDefined();
	});
	it('new samsungPhone property does not alter its prototype', function(){
		samsungPhone.color = 'black';
		expect(samsungPhone.color).toBe('black');
		expect(phone.color).toBe(undefined);
	});
	it('new phone property does alter samsungPhone', function(){
		phone.cpu.frequency = 2.4;
		expect(samsungPhone.cpu.frequency).toBe(2.4);
		expect(phone.cpu.frequency).toBe(2.4);
	});
});