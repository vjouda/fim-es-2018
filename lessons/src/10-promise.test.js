describe('Promise', () => {
    test('promise should resolve with 42', () => {
        // do not modify this line
        let promise = Promise.resolve();

        return promise.then((val) => expect(val).toBe(42));
    });
	test('promise should resolve with 43', () => {
		// do not modify this line
		let promise = Promise.resolve()

		return promise.then((val) => expect(val).toBe(42));
	});
	test('promise should resolve with 44', () => {
		// do not modify this line
		let promise = Promise.reject('err');

		return promise.then((val) => expect(val).toBe(24));
	});
	test('promise should reject', (done) => {
		// do not modify this line
		let promise = Promise.resolve()

		promise.catch(() => done());
	});
});