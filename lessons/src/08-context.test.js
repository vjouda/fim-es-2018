describe('Context', function(){
	function returnContext(){
		return this;
	}
	it('context can be supplied using .call() or .apply() methods', function(){
		var emptyContext = {};
		expect(returnContext.call(emptyContext)).toBe(emptyContext);
	});
	var car = {
		color: 'blue',
		getColor: function(){
			return this.color;
		}
	};
	it("when invoked on object, 'this' is set to given object", function(){
		expect(car.getColor()).toBe('blue');
		car.color = 'red';
		expect(car.getColor()).toBe('red');
		var greenCar = {
			color: 'green'
		};
		expect(car.getColor.call(greenCar)).toBe('green');
	});
	var button = {
		hasBeenClicked: false,
		onClick: function(){
			return this.hasBeenClicked = true;
		}
	};
	var onClickHandler = button.onClick;
	it('context can be lost in some cases', function(){
		onClickHandler();
		expect(button.hasBeenClicked).toBe(false);
	});
	it('but it can be binded to its original context', function(){
		onClickHandler = button.onClick.bind(button);
		onClickHandler();
		expect(button.hasBeenClicked).toBe(true);
	});
});