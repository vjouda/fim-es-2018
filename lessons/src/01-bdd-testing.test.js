describe('Jest BDD testing can', function() {
	it('run individual test cases', function() {
		expect(1).toBe(1);
	});
	it('fail on error', function() {
		expect(1).toBe(2);
	});
	describe('group and nest test cases', () => {
	    test('grouped test case', () => {
		    expect(1).toBe(1);
	    });
	});
});