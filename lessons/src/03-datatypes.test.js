var stringPrimitiveAbc = 'ABC';
var stringWrapperAbc = new String('ABC');

describe('Primitive types should', function(){
	it('be strictly equal by value', function(){
		expect(stringPrimitiveAbc === 'ABC').toBe(true);
	});
	it('not be strictly equal to wrapper type', function(){
		expect(stringWrapperAbc === 'ABC').toBe(false);
	});
	it('be loosely equal to wrapper type', function(){
		expect(stringWrapperAbc == stringPrimitiveAbc).toBe(true);
	});
	it('be strictly equal to valueOf return value', function(){
		expect(stringWrapperAbc.valueOf() == stringPrimitiveAbc).toBe(true);
	})
});

var a = 0.1, b = 0.2;

describe('Because numbers are stored as floating-point', function(){
	it('0.1 + 0.2 should not be equal to 0.3', function(){
		expect(a + b).not.toBe(0.3);
	});
	it('they should be compared using Number.EPSILON', function(){
		expect(Math.abs(a + b - 0.3) < Number.EPSILON).toBe(true);
	});
});

describe('Undefined value should', function(){
	it('be default state of not existing properties', function(){
		var person = {};
		expect(person.name).toBe(undefined);
	});
	it('be assignable', function(){
		var person = {
			name: 'Mr.Mock'
		};
		person.name = undefined;
		// wrong approach instead of deletion, person.hasProperty('name') still returns true
		expect(person.name).toBe(undefined);
	});
});

describe('Null and undefined should', function(){
	it('be loosely equal to each other', function(){
		expect(undefined == null).toBe(true);
	});
	it('not be strictly equal to each other', function(){
		expect(undefined === null).toBe(false);
	});
});