var globalVar = 'global';

function foo() {
	var localVar = 'local';
}

describe('Scope', function() {
	function noLocalScope(array){
		// i++;
		for(var i = 0; i < array.length; i++){
			console.log(array[i]);
		}
		return i;
	}
	it("iteration variable lives out of 'for' block scope", function() {
		expect(noLocalScope(['string'])).toBe(1);
	});
	function callCaptureFactory(){
		var callNr = 0;
		return function() {
			return ++callNr;
		}
	}
	it('captured variable is incremented per call', function() {
		var callCapture = callCaptureFactory();
		expect(callCapture()).toBe(1);
		expect(callCapture()).toBe(2);
	});
	function createObjectWithPrivate(){
		var _privateVar = 'private';
		return {
			getPrivate: function(){
				return _privateVar;
			}
		}
	}
	it('closures can be used to hold private values', function() {
		expect(createObjectWithPrivate()._privateVar).toBeUndefined();
		expect(createObjectWithPrivate().getPrivate()).toBe('private');
	});
});